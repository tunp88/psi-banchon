var psi_game = angular.module('psi_game', []);

psi_game.constant('PsiGameConstant', PsiGameConstant);
psi_game.controller('PsiController', PsiController);

function PsiController($scope, $rootScope, $timeout, PsiGameConstant, $log,
		orderByFilter) {
	$rootScope.selectedIndex = -2;
	$scope.showResult = false;
	$scope.gameContent = false;
	$scope.onStart = onStart;
	$scope.onPlay = onPlay;
	$scope.onFinish = onFinish;
	$scope.exitContentResult = exitContentResult;
	$scope.countScore = countScore;
	$scope.onStart();

	function onStart() {
		$scope.checkpointData = angular.copy(PsiGameConstant.checkPoint);
		$scope.choices = angular.copy(PsiGameConstant.choice);

	}

	function onPlay() {
		$scope.gameContent = true;
	}

	function onFinish() {
		
		$scope.choices = orderByFilter($scope.choices, [ '-score', 'priority' ]);
		
		var choisesByPriority = $scope.choices.slice(0);
		console.log("$scope.choices = " + JSON.stringify(choisesByPriority))
		
		$scope.finishResults = [];
		$scope.finishResults.descriptionContent = "Kết quả phương pháp lựa chọn của bạn là:";
		$scope.finishResults.note = "*Click để biết thêm thông tin chi tiết của phương pháp";
		$scope.finishResults.result = [];
		if(choisesByPriority[choisesByPriority.length-1].code == choisesByPriority[choisesByPriority.length-2].code)
		{
			$scope.finishResults.result.push(choisesByPriority[choisesByPriority.length - 1]);		
			$scope.finishResults.result.push(choisesByPriority[choisesByPriority.length - 2]);
		}
		else
		{
			$scope.finishResults.result.push(choisesByPriority[choisesByPriority.length - 1]);		
		}
		
		$scope.showResult = true;
	}

	function exitContentResult() {
		$scope.showResult = false;
		$rootScope.selectedIndex = -2;
	}

	function countScore(answerCode, value) {
		var choicesSize = $scope.choices.length;
		var choiceColumn = answerCode - 1;
		var i;
		for (i = 0; i < choicesSize; i++) {
			if (value) {
				$scope.choices[i].score += PsiGameConstant.criteria[$scope.choices[i].code - 1][choiceColumn];
			} else {
				$scope.choices[i].score -= PsiGameConstant.criteria[$scope.choices[i].code - 1][choiceColumn];
			}
		}
//		 $log.info('choice: ' + angular.toJson($scope.choices, true));
	}
}

psi_game.directive('checkPoint', checkPointController);

function checkPointController($rootScope, $timeout) {
	var directive = {};

	directive.restrict = 'E';
	directive.scope = {
		checkpointId : '@',
		classActiveParam : '@',
		classDefaultParam : '@',
		checkpointData : '=',
		mainSrcParam : '=',
		descriptionTitle : '=',
		descriptionContent : '=',
		countScore : '='
	};

	directive.templateUrl = 'checkpoint.html';

	directive.controller = function($scope, $rootScope) {
		$scope.enableChoice = true;
		$scope.checkpoint_activated = false;
		$scope.showContent = false;
		$scope.hover = false;
		// $scope.classHideContent = ''; //TODO: check is used?
		$scope.hoverIn = function(checkpointId) {
			$scope.hover = true;
		}

		$scope.hoverOut = function(checkpointId) {
			$timeout(function() {
				$scope.hover = false;
			}, 500);
		}

		$scope.onClickCheckpoint = function() {
			$scope.showContent = true;
			$rootScope.selectedIndex = $scope.checkpointId;
		}

		$scope.onSelectAnswerContent = function(index) {
			if ($scope.enableChoice) {
				$scope.enableChoice = false;
				// change status + count score for each choice.
				var answerContent = $scope.checkpointData.answerContent;
				for (var i = 0; i < answerContent.length; i++) {
					if ((i != index && answerContent[i].selected)
							|| (i == index && !answerContent[i].selected)) {
						answerContent[i].selected = !answerContent[i].selected;
						$scope.countScore(answerContent[i].code,
								answerContent[i].selected);
					}
				}

				if (answerContent[index].selected) {
					$scope.checkpoint_activated = true;
					$timeout(function() {
						$scope.exitContent();
					}, 750)
				} else {
					$scope.checkpoint_activated = false;
				}

				$timeout(function() {
					$scope.enableChoice = true;
				}, 1000);
			}
		}

		$scope.exitContent = function() {
			$scope.showContent = false;
			$rootScope.selectedIndex = -2;
		}
	}

	return directive;
}
